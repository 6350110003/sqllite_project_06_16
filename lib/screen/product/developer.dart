import 'dart:io';

import 'package:flutter/material.dart';


class Developer extends StatefulWidget {
  const Developer({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Developer> createState() => _DeveloperState();
}

class _DeveloperState extends State<Developer> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text('System Member'),

      ),
      body: Container(

        child: ListView(
          children: [
            ListTile(

              leading: CircleAvatar(
                backgroundImage: AssetImage("assets/ma.jpg",),
              ),
              title: const Text('นางสาว ณัสมา สุภาวรรณ์'),
              subtitle: Text('6350110006@psu.ac.th'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            Divider(
              thickness: 0.8,
            ),
            ListTile(

              leading: CircleAvatar(
                backgroundImage: AssetImage("assets/parn.jpg",),
              ),
              title: const Text('นางสาว ยลรดี สุขใส'),
              subtitle: const Text('6350110016@psu.ac.th'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            Divider(
              thickness: 0.8,
            ),

          ],
        ),
      ),
    );
  }
}